export default function ({ store, redirect }) {
  if (process.client) {
    const predata = window.localStorage.getItem('data')
    if (predata !== null) {
      const data = JSON.parse(predata)
      if (Object.keys(data).length !== 0) {
        store.commit('auth/SET_AUTH', data)
        store.dispatch('carts/getCart')
      } else if (store.state.auth.auth.length === 0) {
        return redirect('/login')
      }
    } else {
      return redirect('/login')
    }
  }
}
