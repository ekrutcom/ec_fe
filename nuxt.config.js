
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || 'EKRUT COACH | Platform edukasi online untuk profesional' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { type: 'text/javascript',
        src: 'https://app.sandbox.midtrans.com/snap/snap.js',
        'data-client-key': 'SB-Mid-client-g_s_xtzDozic98gX'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'plyr/dist/plyr.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/currency', ssr: false },
    { src: '~/plugins/vue-carousel', ssr: false },
    { src: '~/plugins/localStorage.js', ssr: false },
    { src: '~/plugins/pdf.js', ssr: false },
    { src: '~/plugins/vue-plyr' },
    { src: '~/plugins/vue-form-wizard' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    '@nuxtjs/proxy',
    '@nuxtjs/universal-storage'
  ],
  toast: {
    duration: 1000
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: true
  },
  proxy: {
    '/midtrans/': { target: 'https://app.sandbox.midtrans.com/snap/v1/transactions', pathRewrite: { '^/midtrans/': '' } }
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['vue-carousel'],
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
