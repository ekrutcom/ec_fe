import axios from 'axios'
// import { response } from 'express'

export const state = () => ({
  auth: [],
  user: []
})
export const getters = {
  getAuth (state) { return state.auth },
  getUser (state) { return state.user }
}
export const mutations = {
  SET_AUTH (state, payload) {
    state.auth = payload
  },
  CLEAR_AUTH (state) {
    state.auth = []
    state.user = []
  },
  SET_DATA (payload) {
    localStorage.setItem('data', payload)
  },
  SET_USER (state, payload) {
    state.user = payload
  }
}
export const actions = {
  login ({ commit, dispatch }, payload) {
    axios
      .post('http://devel.ekrut.com:4000/api/v1/auth-core/signin', payload, { headers: { 'X-App-Source': 'marketplace' } })
      .then((response) => {
        commit('SET_AUTH', response.data)
        const token = String(response.data.data.token)
        localStorage.setItem('token', token)
        const data = JSON.stringify(response.data)
        // const uid = JSON.parse(data).data.id
        // console.log(uid)
        localStorage.setItem('data', data)
        dispatch('getUserDetail')
        dispatch('orders/getOrdersFromApi', null, { root: true })
        dispatch('carts/getCart', null, { root: true })
        this.$router.replace({ path: '/profile/' })
        this.$toast.success('Successfully login')
      }).catch((error) => {
        this.$toast.error(error.response.data.error)
      })
  },
  logout ({ dispatch }) {
    dispatch('clearAllState')
    localStorage.clear()
    this.$toast.error('Logging Out.....')
    this.$router.replace({ path: '/' })
  },
  getUserDetail ({ commit }) {
    const token = String(localStorage.getItem('token'))
    const uid = JSON.parse(localStorage.data).data.id
    axios.get('http://devel.ekrut.com:4002/api/v1/talent/' + uid, { headers: { 'Authorization': token } })
      .then((response) => {
        // console.log(response)
        commit('SET_USER', response.data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  clearAllState ({ commit }) {
    commit('CLEAR_AUTH')
    commit('orders/RESET_ALL_ORDERS', null, { root: true })
    commit('carts/RESET_CART', null, { root: true })
  }
}
