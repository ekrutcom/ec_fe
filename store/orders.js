import axios from 'axios'
// import midtrans from 'midtrans-client'
export const state = () => ({
  order: {},
  orders: [],
  lastOrderId: null,
  orderCourses: []
})
export const getters = {
  getOrder (state) {
    return state.order
  },
  getOrders (state) {
    return state.orders
  },
  getOrderCourses (state) {
    return state.orderCourses
  },
  getOrderCoursesAmount (state) {
    let total = 0
    state.orders.forEach((item) => {
      total += item.total_amount
    })
    return total
  }
}
export const mutations = {
  RESET_ALL_ORDERS (state) {
    state.order = {}
    state.orders = []
    state.orderCourses = []
    state.lastOrderId = null
  },
  SET_ORDER (state, order) {
    state.order = order
  },
  SET_ORDERS (state, orders) {
    state.orders = orders
  },
  SET_OID (state, lastOrderId) {
    state.lastOrderId = lastOrderId
  },
  RESET_OID (state) {
    state.lastOrderId = null
  },
  SET_ORDER_COURSE (state, { orderCourse }) {
    // state.orderCourses = orderCourses
    const courseInCart = state.orderCourses.find((item) => {
      return item.orderCourse.id === orderCourse.id
    })
    if (courseInCart) {
      // this.$toast.error('This course already in the cart')
      return
    }
    state.orderCourses.push({ orderCourse })
  }
}
export const actions = {
  // post order to api
  async postApiOrder ({ commit }, payload) {
    const token = String(localStorage.getItem('token'))
    const response = await axios.post('http://adm.ekrut.co/orders', payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
    return response
  },
  // get order from api
  async getApiOrder ({ commit }, payload) {
    const token = String(localStorage.getItem('token'))
    const response = await axios.get('http://adm.ekrut.co/orders/' + payload, { headers: { 'Content-type': 'text/plain', 'auth-core': token } })
    return response
  },
  // get user order from api
  async getApiUserOrder ({ commit }) {
    const token = String(localStorage.getItem('token'))
    const uid = JSON.parse(localStorage.data).data.id
    const response = await axios.get('http://adm.ekrut.co/orders?user_id=' + uid, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
    return response
  },
  // create order sequence
  async createOrders ({ commit, dispatch }, { courses, total }) {
    // declare
    // const token = String(localStorage.getItem('token'))
    const uid = JSON.parse(localStorage.data).data.id
    const payload = {
      user_id: uid,
      total_amount: total,
      status: 'pending',
      courses
    }
    const oid = parseInt(this.state.orders.lastOrderId) || 0
    console.log(oid)
    if (oid !== 0) {
      try {
        const response = await dispatch('getApiOrder', oid)
        // axios.get('http://adm.ekrut.co/orders/' + oid, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
        commit('SET_ORDER', response.data)
        commit('SET_OID', response.data.id)
        if (response) {
          return response.data
        }
      } catch (error) {
        const response = await dispatch('postApiOrder', payload)
        // axios.post('http://adm.ekrut.co/orders', payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
        await commit('SET_ORDER', response.data)
        await commit('SET_OID', response.data.id)
        if (response) {
          return response.data
        }
      }
    } else {
      const response = await dispatch('postApiOrder', payload)
      // await axios.post('http://adm.ekrut.co/orders', payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
      await commit('SET_ORDER', response.data)
      await commit('SET_OID', response.data.id)
      if (response) {
        return response.data
      }
    }
  },
  // order success sequence
  async orderSuccess ({ commit, dispatch }, payload) {
    commit('RESET_OID')
    if (process.client) {
      const token = String(localStorage.getItem('token'))
      const body = {
        status: 'success'
      }
      const response = await axios.put('http://adm.ekrut.co/orders/' + payload, body, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
      if (response) {
        console.log(response.data.status)
        response.data.courses.forEach((item) => {
          dispatch('carts/removeCourseFromCart', { course: item }, { root: true })
        })
      }
    }
  },
  async createMidtransToken ({ commit }, payload) {
    const response = await this.$axios.post('/midtrans/', payload, { headers: { 'Authorization': 'Basic "U0ItTWlkLXNlcnZlci1qQm44SHgzSXMwZFFIbDdQY1I0WUtybHo="', 'Access-Control-Allow-Origin': '*' } })
    if (response) {
      // console.log(response.data.token)
      return response.data.token
    }
  },
  getOrderFromApi ({ commit }, payload) {
    if (process.client) {
      const token = String(localStorage.getItem('token'))
      axios
        .get('http://adm.ekrut.co/orders/' + payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
        .then(response => response.data)
        .then((order) => {
          commit('SET_ORDER', order)
        })
    }
  },
  async getOrdersFromApi ({ commit, dispatch }) {
    if (process.client) {
      // const token = String(localStorage.getItem('token'))
      // const uid = JSON.parse(localStorage.data).data.id
      const response = await dispatch('getApiUserOrder')
      // axios.get('http://adm.ekrut.co/orders?user_id=' + uid, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
      if (response) {
        commit('SET_ORDERS', response.data)
        const x = JSON.stringify(response.data)
        const data = JSON.parse(x)
        // console.log(data)
        data.forEach((element) => {
          if (element.status === 'success') {
            element.courses.forEach((x) => {
              commit('SET_ORDER_COURSE', { orderCourse: x })
            })
          }
        })
      }
    }
  },
  checkCourseInOrders ({ state, dispatch }, { course }) {
    const courseInOrder = state.orderCourses.find((item) => {
      return item.orderCourse.id === course.id
    })
    if (courseInOrder) {
      this.$toast.error('You Have Purchase This Course Before')
    } else {
      dispatch('carts/addCourseToCart', { course }, { root: true })
    }
  },
  checkCourseValid ({ state, dispatch }, payload) {
    const courseInOrder = state.orderCourses.find((item) => {
      return item.orderCourse.id === parseInt(payload)
    })
    if (courseInOrder) {
      dispatch('courses/getCourseById', payload, { root: true })
    } else {
      this.$router.back()
      this.$toast.error('UNAUTHORIZED ACCESS')
    }
  }
}
