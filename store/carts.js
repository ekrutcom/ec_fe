import axios from 'axios'
export const state = () => ({
  cart: []
})
export const getters = {
  getCart (state) { return state.cart },
  getCartTotal (state) {
    let total = 0
    state.cart.forEach((item) => {
      total += item.course.price
    })
    return total
  },
  getCartCourse (state) { return state.cart.course }
}
export const mutations = {
  RESET_CART (state) {
    state.cart = []
  },
  SET_CART (state, data) {
    state.cart = data
  },
  // duplicate method need refactor
  API_TO_CART (state, { course }) {
    const courseInCart = state.cart.find((item) => {
      return item.course.id === course.id
    })
    if (courseInCart) {
      return
    }
    state.cart.push({ course })
  },
  ADD_TO_CART (state, { course }) {
    const courseInCart = state.cart.find((item) => {
      return item.course.id === course.id
    })
    if (courseInCart) {
      this.$toast.error('This Course Already in The Cart')
      return
    }
    this.$toast.success('Successfully Added Course To Cart')
    state.cart.push({ course })
  },
  REMOVE_FROM_CART (state, { course }) {
    state.cart = state.cart.filter((item) => {
      return item.course.id !== course.id
    })
    this.$toast.success('Succesfully Remove ' + course.title)
  }
}
export const actions = {
  async getApiCart () {
    const uid = JSON.parse(localStorage.data).data.id
    const token = String(localStorage.getItem('token'))
    const response = await axios.get('http://adm.ekrut.co/carts?user_id=' + uid, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
    return response
  },
  async postApiCart (payload) {
    const token = String(localStorage.getItem('token'))
    const response = await axios.post('http://adm.ekrut.co/carts', payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
    return response
  },
  async putApiCart (context, { id, payload }) {
    const token = String(localStorage.getItem('token'))
    const response = await axios.put('http://adm.ekrut.co/carts/' + id, payload, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
    return response
  },
  async syncCartToApi ({ state, dispatch }) {
    const uid = JSON.parse(localStorage.data).data.id
    const cartCourse = state.cart
    const courses = []
    cartCourse.forEach((element) => {
      courses.push(element.course)
    })
    const payload = {
      user_id: uid,
      courses
    }
    const result = await dispatch('getApiCart')
    if (result) {
      if (result.data.length === 0) {
        await dispatch('postApiCart', payload)
      } else {
        const cartId = result.data[0].id
        await dispatch('putApiCart', { id: cartId, payload })
      }
    }
  },
  async getCart ({ commit, dispatch }) {
    const response = await dispatch('getApiCart')
    if (response) {
      const courses = response.data[0].courses
      courses.forEach((element) => {
        commit('API_TO_CART', { course: element })
      })
    } else {
      console.log('Error getApiCart')
    }
  },
  async addCourseToCart ({ commit, rootState, dispatch }, { course }) {
    await commit('ADD_TO_CART', { course })
    if (typeof rootState.auth.auth.data.id !== 'undefined') {
      if (rootState.auth.auth.data.id !== null) {
        await dispatch('syncCartToApi')
      }
    }
  },
  async removeCourseFromCart ({ commit, rootState, dispatch }, { course }) {
    commit('REMOVE_FROM_CART', { course })
    if (typeof rootState.auth.auth.data.id !== 'undefined') {
      if (rootState.auth.auth.data.id !== null) {
        await dispatch('syncCartToApi')
      }
    }
  }
}
