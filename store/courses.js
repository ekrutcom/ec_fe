import axios from 'axios'

export const state = () => ({
  courses: [],
  course: {}
})

export const getters = {
  getCourses (state) { return state.courses },
  getCourse (state) { return state.course }
}
export const mutations = {
  SET_COURSES (state, data) {
    state.courses = data
  },
  SET_COURSE (state, data) {
    state.course = data
  }
}
export const actions = {

  getAllCourses ({ commit }) {
    axios
      .get('http://ekrut.co:1337/courses')
      .then(response => response.data)
      .then((courses) => {
        commit('SET_COURSES', courses)
      })
  },
  getCourseById ({ commit }, payload) {
    axios
      .get(`http://ekrut.co:1337/courses/${payload}`)
      .then(response => response.data)
      .then((course) => {
        commit('SET_COURSE', course)
      })
  }
}
