import axios from 'axios'
export const state = () => ({})
export const getters = {}
export const mutations = {}
export const actions = {
  async getApiProgress (context, { courseId, topicId }) {
    const uid = JSON.parse(localStorage.data).data.id
    const token = String(localStorage.getItem('token'))
    const cid = courseId
    const tid = topicId
    try {
      const response = await axios.get('http://adm.ekrut.co/progress?user_id=' + uid + '&course_id=' + cid + '&topic_id=' + tid, { headers: { 'Content-Type': 'text/plain', 'auth-core': token } })
      if (response) {
        return response
      }
    } catch (error) {

    }
  },
  async postApiProgress ({ commit }, { courseId, topicId }) {
    const uid = JSON.parse(localStorage.data).data.id
    const token = String(localStorage.getItem('token'))
    const data = {
      user_id: uid,
      course_id: courseId,
      topic_id: topicId
    }
    const response = await axios.post('http://adm.ekrut.co/progresses', data, { headers: { 'Content-type': 'text/plain', 'auth-core': token } })
    if (response) {
      // return
    }
  },
  changeProgress ({ context, dispatch }, { courseId, topicId }) {
    return new Promise(async (resolve, reject) => {
      const result = await dispatch('getApiProgress', { courseId, topicId })
      if (!result) {
        resolve(true)
        await dispatch('postApiProgress', { courseId, topicId })
      } else {
        resolve(true)
      }
    })
  }
}
